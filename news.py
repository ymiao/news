"""NVDM Tensorflow implementation by Yishu Miao"""
from __future__ import print_function

import numpy as np
import tensorflow as tf
import math
import os
import utils 
import data_utils

np.random.seed(0)
tf.set_random_seed(0)

flags = tf.app.flags
flags.DEFINE_string('data_dir', 'data/', 'Data dir path.')
flags.DEFINE_string('train_dir', '/home/ymiao/Work/Data/train_dir/news', 'Train dir path.')
flags.DEFINE_float('learning_rate', 1e-5, 'Learning rate.')
flags.DEFINE_integer('batch_size', 64, 'Batch size.')
flags.DEFINE_integer('n_hidden', 256, 'Size of each hidden layer.')
flags.DEFINE_integer('n_topic', 200, 'Size of stochastic vector.')
flags.DEFINE_integer('n_sample', 1, 'Number of samples.')
flags.DEFINE_integer('vocab_size', 30000, 'Vocabulary size.')
flags.DEFINE_boolean('test', False, 'Process test data.')
flags.DEFINE_string('non_linearity', 'tanh', 'Non-linearity of the MLP.')
FLAGS = flags.FLAGS

class NTTM(object):
    """ Neural Variational Document Model -- BOW VAE.
    """
    def __init__(self, 
                 vocab_size,
                 n_hidden,
                 n_topic, 
                 n_sample,
                 learning_rate, 
                 batch_size,
                 non_linearity):
        self.vocab_size = vocab_size
        self.n_hidden = n_hidden
        self.n_topic = n_topic
        self.n_sample = n_sample
        self.non_linearity = non_linearity
        self.learning_rate = learning_rate
        

        # input
        self.x = tf.sparse_placeholder(tf.float32, shape=[None, vocab_size], name='input')
        self.x_dense = tf.sparse_tensor_to_dense(self.x, validate_indices=False)         
        
        self.eps = tf.placeholder(tf.float32, [None, n_topic], name='eps')
   
        self.lengths = tf.placeholder(tf.float32, [None], name='lengths')
        self.mask = 1 - tf.cast(tf.equal(self.lengths, 0), tf.float32)

        self.history = tf.placeholder(tf.float32, [100, 1, n_hidden], name='history')
        self.year = tf.placeholder(tf.int32, name='year')
        self.month = tf.placeholder(tf.int32, name='month')
        self.day = tf.placeholder(tf.int32, name='day')

        #print(self.year)
        #print(tf.nn.embedding_lookup(emb_year, self.year))
        
        with tf.variable_scope('encoder'): 
        # prior
          emb_year = tf.get_variable('emb_year', [7, n_hidden/4], dtype=tf.float32)
          emb_month = tf.get_variable('emb_month', [12, n_hidden/4], dtype=tf.float32)
          emb_day = tf.get_variable('emb_day', [31, n_hidden/4], dtype=tf.float32)
          emb_date = tf.get_variable('emb_date', [7, n_hidden/4], dtype=tf.float32)

          self.time_input = tf.stack([tf.nn.embedding_lookup(emb_year, self.year),
                                     tf.nn.embedding_lookup(emb_month, self.month),
                                     tf.nn.embedding_lookup(emb_day, self.day),
                                     tf.nn.embedding_lookup(emb_date, self.year)])
          self.time_input = tf.reshape(self.time_input, [n_hidden])
          history_list = tf.unstack(self.history, axis=0)
          time_inputs = history_list[1:] + [tf.expand_dims(self.time_input, axis=0)]

        
          cell = utils._lstm_cell(self.n_hidden, 1)
          outputs, state =  tf.contrib.rnn.static_rnn(cell,
                                                      inputs=time_inputs,
                                                      dtype=tf.float32,
                                                      scope='rnn_prior' )
          self.mean_p = utils.linear(outputs[-1], self.n_topic, scope='mean_p')
          self.logsigm_p = utils.linear(outputs[-1], self.n_topic, scope='logsigm_p')

        # encoder
          w1 = tf.get_variable('w1', [vocab_size, n_hidden], dtype=tf.float32)
          b1 = tf.get_variable('b1', [n_hidden], dtype=tf.float32) 
          self.x_rep = tf.sparse_tensor_dense_matmul(self.x, w1) + b1
          self.enc_vec = utils.mlp(self.x_rep, [self.n_hidden], self.non_linearity)
          self.mean = utils.linear(self.enc_vec, self.n_topic, scope='mean')
          self.logsigm = utils.linear(self.enc_vec, 
                                     self.n_topic, 
                                     bias_start_zero=True,
                                     matrix_start_zero=True,
                                     scope='logsigm')
          #self.kld = -0.5 * tf.reduce_sum(1 - tf.square(self.mean) + 2 * self.logsigm - tf.exp(2 * self.logsigm), 1)
          self.kld = -0.5 * tf.reduce_sum(
                1 - (tf.square(self.mean- self.mean_p) + tf.exp(2*self.logsigm))/tf.exp(2*self.logsigm_p) + 2*self.logsigm - 2*self.logsigm_p , 1)

          self.kld = self.mask*self.kld  # mask paddings
        
        with tf.variable_scope('decoder'):

          w2 = tf.get_variable('w2', [n_topic, vocab_size], dtype=tf.float32)
          b2 = tf.get_variable('b2', [vocab_size], dtype=tf.float32) 

          if self.n_sample ==1:  # single sample
            doc_vec = tf.exp(self.logsigm)*self.eps + self.mean
            logits = tf.nn.log_softmax(tf.matmul(doc_vec, w2) + b2)
            self.recons_loss = -tf.reduce_sum(logits * self.x_dense, 1)
          # multiple samples
          else:
            eps_list = tf.split(0, self.n_sample, self.eps)
            recons_loss_list = []
            for i in xrange(self.n_sample):
              if i > 0: tf.get_variable_scope().reuse_variables()
              curr_eps = eps_list[i]
              doc_vec = tf.mul(tf.exp(self.logsigm), curr_eps) + self.mean
              logits = tf.nn.log_softmax(tf.matmul(doc_vec, w2) + b2)
              recons_loss_list.append(-tf.reduce_sum(tf.mul(logits, self.x_dense), 1))
            self.recons_loss = tf.add_n(recons_loss_list) / self.n_sample

        self.objective = self.recons_loss + self.kld
        self.new_history = tf.stack(time_inputs, axis=0)

        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        fullvars = tf.trainable_variables()
        #self.optim = optimizer.minimize(self.objective)
        self.topics = w2

        enc_vars = utils.variable_parser(fullvars, 'encoder')
        dec_vars = utils.variable_parser(fullvars, 'decoder')

        enc_grads = tf.gradients(self.objective, enc_vars)
        dec_grads = tf.gradients(self.objective, dec_vars)

        self.optim_enc = optimizer.apply_gradients(zip(enc_grads, enc_vars))
        self.optim_dec = optimizer.apply_gradients(zip(dec_grads, dec_vars))

def train(sess, model, 
          train_url, 
          test_url, 
          batch_size, 
          training_epochs=100, 
          alternate_epochs=1):
  """train nvdm model."""
  
  data = data_utils.read_sentences(train_url)
  
  for epoch in range(training_epochs):
    total_batch = len(data[0])
    print('Train batches: ' + str(total_batch))  
    #-------------------------------
    # train
    for switch in xrange(0, 2):
      if switch == 0:
        optim = model.optim_dec
        print_mode = 'updating decoder'
      else:
        optim = model.optim_enc
        print_mode = 'updating encoder'

      for a in xrange(alternate_epochs):

        loss_sum = 0.0
        ppx_sum = 0.0
        kld_sum = 0.0
        word_count = 0

        history = np.zeros([100, 1, FLAGS.n_hidden])

        for n in xrange(total_batch):

          time, batch_inputs, batch_lengths = data_utils.fetch_batch(data, n, FLAGS.vocab_size)

          batch_size = len(batch_lengths) 

          input_feed = {model.x: batch_inputs, 
                        model.lengths: batch_lengths,
                        model.eps: np.random.normal(0,1,[batch_size,FLAGS.n_topic]),
                        model.history: history,
                        model.year: time[0],
                        model.month: time[1],
                        model.day: time[2],}
          
          _, (loss, kld, history, topics) = sess.run((optim, 
              [model.objective, model.kld, model.new_history, model.topics]), input_feed)

          loss_sum += np.sum(loss)
          kld_sum += np.sum(kld) / batch_size
          word_count += np.sum(batch_lengths)


        print_ppx = np.exp(loss_sum / word_count)
        print_kld = kld_sum/total_batch
        print('| Epoch train: {:d} |'.format(epoch+1), 
              print_mode, '{:d}'.format(a),
              '| Corpus ppx: {:.5f}'.format(print_ppx),  # perplexity for all docs
              '| KLD: {:.5}'.format(print_kld))
        #-------------------------------
    save_topics(topics, os.path.join(FLAGS.train_dir, 'topics.'+str(epoch)))


def save_topics(topics, save_path):

  save_file = tf.gfile.GFile(save_path, mode='w')
  voc_file = tf.gfile.GFile(os.path.join(FLAGS.data_dir, 'vocab.txt'))

  voc = []
  while True:
    word = voc_file.readline()
    if not word:
      break
    voc.append(word.strip().split()[0])
  voc_file.close()

  for k, vec in enumerate(topics):
    rank = np.argsort(-vec)
    save_file.write('Topic '+str(k)+':\n')
    for j in xrange(0, 200):
      save_file.write(voc[rank[j]]+' '+ str(vec[rank[j]])+' ')
    save_file.write('\n\n')    
  save_file.close()

def main(argv=None):
    if FLAGS.non_linearity == 'tanh':
      non_linearity = tf.nn.tanh
    elif FLAGS.non_linearity == 'sigmoid':
      non_linearity = tf.nn.sigmoid
    else:
      non_linearity = tf.nn.relu

    nttm = NTTM(vocab_size=FLAGS.vocab_size,
                n_hidden=FLAGS.n_hidden,
                n_topic=FLAGS.n_topic, 
                n_sample=FLAGS.n_sample,
                learning_rate=FLAGS.learning_rate, 
                batch_size=FLAGS.batch_size,
                non_linearity=non_linearity)
    sess = tf.Session()
    init = tf.initialize_all_variables()
    sess.run(init)

    train_url = os.path.join(FLAGS.data_dir, 'abcnews.ids')
    test_url = os.path.join(FLAGS.data_dir, 'abcnews.ids') 

    train(sess, nttm, train_url, test_url, FLAGS.batch_size)

if __name__ == '__main__':
    tf.app.run()
