from __future__ import print_function
import random
import numpy as np
import tensorflow as tf
import locale
from collections import defaultdict
locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' )

def read_sentences(source_path):

  time_set = []
  id_set = []


  with tf.gfile.GFile(source_path, mode='r') as source_open:
    sent = source_open.readline()
    prev_time = ''
    ids = []
    while sent:
      #print(sent)
      elements = sent.strip().split('|||')

      times = elements[0].split()
      words = elements[1].split()
      ids.append(words)

      if prev_time != elements[0]: # new time
        time_set.append(times)
        id_set.append(ids)
        ids = []
        prev_time = elements[0]
      sent = source_open.readline()

  id_set.pop(0)
  id_set.append(ids)

  data = (time_set, id_set)

  return data

def fetch_batch(data, idx, vocab_size=30000):
  """fetch a batch of data by given idx_chunk."""
  (time_set, id_set) = data

  time = time_set[idx]
  ids = id_set[idx]
  num_row = len(ids)
    
  idx_map = defaultdict(int)
  
  indices = []
  values = []
  batch_lengths = []

  # Batch encoder inputs are just re-indexed inputs.
  for n, ids_row in enumerate(ids):
    length = 0
    for word_id in ids_row:
      idx_map[(n, word_id)] += 1
      length +=1
    batch_lengths.append(length)

  for row, word_id in idx_map:
    #print(key)
    indices.append([row,word_id])
    values.append(idx_map[(row, word_id)])

  indices = np.array(indices, dtype=np.int32)
  values = np.array(values, dtype=np.int32)
  shape = np.array([num_row, vocab_size], dtype=np.int32)

  batch_inputs = (indices, values, shape)
  

  return (time,           # year, month, day
          batch_inputs,   # indices, value
          batch_lengths)  # 

def main():
  source_path = 'data/abcnews.ids'
  data = read_sentences(source_path)
  print(data[0][0])
  print(data[1][0])
  input1 = fetch_batch(data, 0, 30000)
  print(input1)

if __name__ == '__main__':
  main()