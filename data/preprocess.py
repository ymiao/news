import sys
from collections import defaultdict
from nltk.tokenize import word_tokenize

dir_path = './'
text_file = 'abcnews-date-text.csv'
ids_file = 'abcnews.ids'
voc_file = 'vocab.txt' 
stop_file = 'stopword.txt'

def save_voc(text_file, voc_file=None, stop_file=None, ids_file=None, vocab_size=30000):
    data = []

    #--construct stopvoc
    stopvoc = []
    # read stopword
    if stop_file:
        fin = open(stop_file, 'r')
        while True:
            stopword = fin.readline().strip()
            if not stopword:
                break
            stopvoc.append(stopword)
        fin.close()
    
    
    word_freq = defaultdict(int)
    # read voc
    fin = open(text_file,'r')
    while True:
        line = fin.readline()
        if not line:
            break
        items = line.split(',')
        time = items[0]
        #print(time)
        text = ' '.join(items[1:])
        words = word_tokenize(items[1].strip())
        for word in words:
            if word.isdigit():
                word_freq['NUM'] += 1
            else:
                word_freq[word] += 1
        data.append((time, words))

    # filter vocab
    word2id = {}
    voc_new = []
    word_freq = sorted(word_freq.iteritems(), key=lambda (k,v): (v,k), reverse=True)    
    count = 0
    for (k,v) in word_freq:
        if k not in stopvoc:
            voc_new.append((k,v))
            word2id[k]=len(word2id)
        if len(voc_new) >= vocab_size:
                break
    # save voc
    fout = open(voc_file, 'w')
    for word,freq in voc_new:
        fout.write(word+' '+ str(freq) +'\n')
    fout.close()

    # save ids
    fout = open(ids_file, 'w')
    # year, month, date, day
    #print(data)
    for time, words in data:
        #print(time)
        #print(words)
        fout.write(str(int(time[:4])-2003)+' ' + str(int(time[4:6])-1) + ' ' + str(int(time[6:8])-1) + ' ||| ')
        ids = ''
        for word in words:
            if word.isdigit():
                word = 'NUM'
            if word in word2id:
                ids += str(word2id[word]) + ' '
        fout.write(ids +'\n')
    fout.close()

save_voc(dir_path+text_file, dir_path+voc_file, dir_path+stop_file, dir_path+ids_file)

